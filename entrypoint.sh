#!/bin/bash
apt update && apt install apache2 git -y
git clone https://gitlab.com/santiagopereyra2702/educacionit-docker.git /app
rm -r /var/www/html
cp -r /app /var/www/html
sed -i "s/My App/$SITENAME/" /var/www/html/index.html
exec "$@"
